# surf

Simple Ultra Reduced Finder (surf) is a one-window web browser based on WebKitGTK[^1].

## Introduction

The goal is to develop and maintain a highly reduced web browser that outsources every possible functionality to external tools.

Examples:

Function                        | External tool
--------------------------------|--------------
bookmarks                       | dmenu or similiar tool
changing to a different website | quite and reopen surf
tabs                            | window manager

surf is inspired by

* "original" surf[^2]
* One-Window Browser WebKIT tutorial[^3]

## FAQ

### Why such a weird name?

There already exist a browser called surf[^2].

But as I like to open a website by typing `surf example.com` in a terminal, I haved to find a different name with the same abbreviation.

### Why reimplement surf?

The "original" surf[^2] doesn't run under Wayland because of features not in scope of this surf (e.g. browsing by changing XProperties).

## References

[^1]: [WebKitGTK](https://webkitgtk.org/)
[^2]: [surf by suckless.org](https://surf.suckless.org/)
[^3]: [One-Window Browser WebKIT tutorial](https://wiki.gnome.org/Projects/WebKitGtk/ProgrammingGuide/Tutorial)
